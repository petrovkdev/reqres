## Getting started

### Compile code
```
make javac
```

### Compile docs
```
make javadoc
```

### Run app
```
java -cp ./out:./src/libs/* ru.reqres.UserCase [Integer argument]
```
### Example
```
java -cp ./out:./src/libs/* ru.reqres.UserCase 1
```