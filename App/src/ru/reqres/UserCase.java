package ru.reqres;

import java.io.*;
import ru.reqres.config.*;
import ru.reqres.model.*;
import ru.reqres.api.*;
import ru.reqres.middlewares.*;

/**
 * User Experience Class
 * @author Petr Klyukin {@literal <petrovk86@mail.ru>}
 */
public class UserCase {

    /**
     * Point of entry to the application
     * @param args One value is allowed. The value must be an integer.
     */
    public static void main(String[] args) {
        Config conf = new Config();

        UserMiddleware userMiddleware = new UserMiddleware();

        Api api = new Api(conf);

        try {
            userMiddleware.inputCheck(args);

            int userId = userMiddleware.getUserId(args);

            InputStream userResponse = api.getUserById(userId);

            BufferedReader response = new BufferedReader(new InputStreamReader(userResponse));

            User.Data userData = userMiddleware.jsonToUserData(response.readLine());

            System.out.println(userData.getFirstName() + " " + userData.getLastName());

        } catch (NumberFormatException | IOException e) {
            System.out.println(e.getMessage());
        }
    }
}
