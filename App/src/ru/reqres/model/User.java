package ru.reqres.model;

/**
 * User Class Model
 * @author Petr Klyukin {@literal <petrovk86@mail.ru>}
 */
public class User {

    /**
     * Data Model
     */
    private final Data data;

    /**
     * Constructor
     * @param data Data Model
     */
    public User(Data data) {
        this.data = data;
    }

    /**
     * Gets Data Model
     * @return Data Model
     */
    public Data getData() {
        return this.data;
    }

    /**
     * Nested class Data Model
     */
    public static class Data {

        /**
         * First name
         */
        private final String first_name;

        /**
         * Last name
         */
        private final String last_name;

        /**
         * Constructor
         * @param first_name First name
         * @param last_name Last name
         */
        public Data(String first_name, String last_name) {
            this.first_name = first_name;
            this.last_name = last_name;
        }

        /**
         * Get First name
         * @return first_name
         */
        public String getFirstName() {
            return this.first_name;
        }

        /**
         * Get Last name
         * @return last_name
         */
        public String getLastName() {
            return this.last_name;
        }
    }
}
