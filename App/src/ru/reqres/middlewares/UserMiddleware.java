package ru.reqres.middlewares;

import java.io.*;
import com.google.gson.*;
import ru.reqres.model.*;

/**
 * Class intermediate layer for working with the user
 * @author Petr Klyukin {@literal <petrovk86@mail.ru>}
 */
public class UserMiddleware {

    /**
     * Checks the number of characters
     * @param args array chars
     * @throws IOException Input Output error
     */
    public void inputCheck(String[] args) throws IOException {
        if (args.length > 1) {
            throw new IOException("Invalid. There can be one value!");
        }
    }

    /**
     * Get user id
     * @param args array chars
     * @return user id
     * @throws NumberFormatException Exclusion of the numeric format
     */
    public int getUserId(String[] args) throws NumberFormatException {
        int id = 0;

        try {
            id = Integer.parseInt(args[0]);
        } catch (NumberFormatException e) {
            throw new NumberFormatException("Invalid. Specify an integer!");
        }

        return id;
    }

    /**
     * Get User Data Model
     * @param responseJson string json
     * @return User Data Model
     */
    public User.Data jsonToUserData(String responseJson) {
        Gson gson = new Gson();
        User user = gson.fromJson(responseJson, User.class);
        return user.getData();
    }
}
