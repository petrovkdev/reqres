package ru.reqres.api;

import ru.reqres.config.*;
import java.io.*;
import java.net.*;

/**
 * API for reqres work
 * @author Petr Klyukin {@literal <petrovk86@mail.ru>}
 */
public class Api {

    /**
     * Config object
     */
    private final Config config;

    /**
     * Constructor
     * @param config Config object
     */
    public Api(Config config) {
        this.config = config;
    }

    /**
     * Get response user
     * @param userId user identifier
     * @return InputStream
     * @throws IOException Input Output error
     */
    public InputStream getUserById(int userId) throws IOException {
        String service = this.config.getHostApi() + "users/" + userId;
        URL url = new URL(service);
        HttpURLConnection reqres = (HttpURLConnection) url.openConnection();
        reqres.disconnect();

        if (reqres.getResponseCode() == 404) {
            throw new IOException("User not found!");
        }

        return reqres.getInputStream();
    }

}
