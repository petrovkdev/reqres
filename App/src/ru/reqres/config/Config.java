package ru.reqres.config;

/**
 * Configuration APP
 * @author Petr Klyukin {@literal <petrovk86@mail.ru>}
 */
public class Config {
    /**
     * Host reqres to API
     */
    private String hostApi = "https://reqres.in/api/";

    /**
     * Get host api
     * @return Host API
     */
    public String getHostApi() {
        return this.hostApi;
    }

    /**
     * Set host api
     * @param hostApi Host reqres api
     */
    public void setHostApi(String hostApi) {
        this.hostApi = hostApi;
    }
}
